import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import api from './service/api'
import {ProblemDisplay} from './components/ProblemsDisplay'
import {OutputDisplay} from './components/OutputDisplay'
import './app.css'


class App extends Component {
    state = {
        problems: [],
        handler: () => {}
    };

    constructor(props) {
        super(props);

        api.problems.all().then(res => {
            this.setState({problems: res.problems})
        });
    }

    render() {
        return (
            <div className="App">
                <OutputDisplay
                    callback={h => this.setState({handler: h})}
                />
                <ProblemDisplay
                    problems={this.state.problems}
                    propagateState={problems => {
                        this.setState({problems})
                    }}
                    handler={this.state.handler}
                />
            </div>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('app'));
