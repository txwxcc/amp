import React, {Component} from 'react';


export class InputText extends Component {
    constructor(props) {
        super(props);

        this.state = {value: props.value, old: props.value};
    }

    validate(oldOne, callback) {
        if (this.state.value !== oldOne && this.state.name !== "")
            callback(this.state.value)
    }

    static getDerivedStateFromProps(props, state) {
        if (props.value !== state.old) {
            return {value: props.value, old: props.value};
        }
        else
            return null
    }

    render() {
        return (
            <input
                type="text"
                value={this.state.value}
                onChange={event => this.setState({value: event.target.value})}
                onBlur={event => this.validate(this.props.value, this.props.change)}
            />
        );
    }
}
