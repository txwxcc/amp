import React, {Component} from 'react';
import {AttributeView} from "../../components/AttributeView";
import {InputText} from "../../components/InputText";
import {Button} from "../../../Button";


export class AspectView extends Component {
    changeName(aspect, updateAspect) {
        return newOne => updateAspect(aspect, {...aspect, name: newOne})
    }

    createAttribute(aspect, updateAspect) {
        if (!aspect.attributes.includes(""))
            updateAspect(aspect, {
                ...aspect,
                attributes: aspect.attributes.concat("")
            })
    }

    updateAttribute(aspect, updateAspect) {
        return (oldOne, newOne) => {
            let c = 0;
            updateAspect(aspect, {
                ...aspect,
                attributes: aspect.attributes.map(a => {
                    if(a !== oldOne || c > 0)
                        return a;
                    else {
                        c = 1;
                        return newOne
                    }
                })
            })
        }
    }

    deleteAttribute(aspect, updateAspect) {
        return (attributes) =>
            updateAspect(aspect, {
                ...aspect,
                attributes: aspect.attributes.filter(a => a !== attributes)
            })
    }

    render() {
        const mutators = {
            updateAttribute: this.updateAttribute(this.props.aspect, this.props.updateAspect),
            deleteAttribute: this.deleteAttribute(this.props.aspect, this.props.updateAspect)
        };

        return (
            <div className="AspectView">
                <InputText
                    key={this.props.aspect.name}
                    value={this.props.aspect.name}
                    change={this.changeName(this.props.aspect, this.props.updateAspect)} />
                <Button
                    text="Add Attribute"
                    action={() => this.createAttribute(this.props.aspect, this.props.updateAspect)}/>
                <Button
                    text="Delete Aspect"
                    action={() => this.props.deleteAspect(this.props.aspect)}/>

                {this.props.aspect.attributes.map((e, i) =>
                    <AttributeView key={i} attribute={e} {...mutators} />)}
            </div>
        );
    }
}
