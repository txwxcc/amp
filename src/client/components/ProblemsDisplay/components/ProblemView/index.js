import React, {Component} from 'react';
import {InputText} from "../../components/InputText";
import {Button} from "../../../Button";
import {AspectView} from "../AspectView";


export class ProblemView extends Component {
    changeName(problem, updateProblem) {
        return newOne => updateProblem({...problem, name: newOne})
    }

    createAspect(problem, updateProblem) {
        // this.problem.aspects =
        //     this.problem.aspects.concat({name: '', attributes: []})
        updateProblem({
            ...problem,
            aspects: problem.aspects.concat({name: "", attributes: []})
        })
    }

    updateAspect(problem, updateProblem) {
        return (oldOne, newOne) => {
            console.log({oldOne, newOne, aspects: problem.aspects.map(a => {
                    if (a !== oldOne)
                        return a;
                    else
                        return newOne
                })});
            updateProblem({
                ...problem,
                aspects: problem.aspects.map(a => {
                    if (a !== oldOne)
                        return a;
                    else
                        return newOne
                })
            })
        }
    }

    deleteAspect(problem, updateProblem) {
        return (aspect) =>
            updateProblem({
                ...problem,
                aspects: problem.aspects.filter(a => a !== aspect)
            })
    }

    render() {
        const mutators = {
            updateAspect: this.updateAspect(this.props.problem, this.props.updateProblem),
            deleteAspect: this.deleteAspect(this.props.problem, this.props.updateProblem)
        };

        return (
            <div className="ProblemView">
                <InputText
                    value={this.props.problem.name}
                    change={this.changeName(this.props.problem, this.props.updateProblem)} />
                <Button
                    text="Add Aspect"
                    action={() => this.createAspect(this.props.problem, this.props.updateProblem)}/>
                <Button
                    text="Delete Problem"
                    action={() => this.props.deleteProblem(this.props.problem)}/>
                <Button
                    text="Use to generate output"
                    action={() => this.props.handler(this.props.problem)}/>

                {this.props.problem.aspects.map((e, i) =>
                    <AspectView key={i} aspect={e} {...mutators} />)}
            </div>
        );
    }
}
