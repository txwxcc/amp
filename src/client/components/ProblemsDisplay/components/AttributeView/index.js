import React, {Component} from 'react';
import {InputText} from "../../components/InputText";
import {Button} from "../../../Button";


export class AttributeView extends Component {
    changeName(attribute, updateAttribute) {
        return newOne => updateAttribute(attribute, newOne)
    }

    render() {
        return (
            <div className="AttributeView">
                <InputText
                    key={this.props.attribute}
                    value={this.props.attribute}
                    change={this.changeName(this.props.attribute, this.props.updateAttribute)} />
                <Button
                    text="Delete Attribute"
                    action={() => this.props.deleteAttribute(this.props.attribute)}/>
            </div>
        );
    }
}
