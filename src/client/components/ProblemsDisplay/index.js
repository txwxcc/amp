import React, {Component} from 'react';
import './style.css'
import api from "../../service/api";
import {ProblemView} from "./components/ProblemView";
import {Button} from "../Button";


export class ProblemDisplay extends Component {
    constructor(props) {
        super(props);

        this.propagateState = this.props.propagateState;
    }

    createProblem() {
        api.problems.add({name: "", aspects: []})
            .then(e => api.problems.all()
                .then(e => this.propagateState(e.problems))
            );
    }

    updateProblem(problems) {
        return (problem) => {
            api.problems.update(problem._id, problem)
                // .then(e => api.problems.all()
                //     .then(e => this.propagateState(e.problems))
                // );}
                .then(e => this.propagateState(
                    problems.map(p => {
                        if(p._id !== problem._id)
                            return p;
                        else
                            return problem
                    })
                ));}
    }

    deleteProblem(problems) {
        return (problem) =>
            api.problems.remove(problem._id)
                // .then(e => api.problems.all()
                //     .then(e => this.propagateState(e.problems))
                // );
                .then(e => this.propagateState(
                    problems.filter(p => p._id !== problem._id)
                ));
    }

    render() {
        const mutators = {
            updateProblem: this.updateProblem(this.props.problems),
            deleteProblem: this.deleteProblem(this.props.problems),
            handler: this.props.handler
        };

        return (
            <div className="ProblemDisplay">
                <Button
                    text="Create Problem"
                    action={() => this.createProblem()}/>

                {this.props.problems.map((e, i) =>
                    <ProblemView key={i} problem={e} {...mutators} />)}
            </div>
        );
    }
}

