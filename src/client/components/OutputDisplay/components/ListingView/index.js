import React, {Component} from 'react';
import {Button} from "../../../Button";


export class ListingView extends Component {
    render() {
        return (
            <div className="ListingView">
                <div>
                    {this.props.listing.map(l =>
                        `${l.aspect}[${l.attribute}],`)}
                </div>
                <Button
                    text="Mark as favorite"
                    action={() => this.props.markAsFavorite(this.props.listing)}/>
            </div>
        );
    }
}
