import React, {Component} from 'react';
import {Button} from "../../../Button";


export class FavoriteView extends Component {
    render() {
        return (
            <div className="FavoriteView">
                {this.props.favorite.map((l, i) =>
                    <div key={i}>
                        {l.list.map(e => `${e.aspect}[${e.attribute}],`)}
                        <Button
                            text="Delete favorite"
                            action={() => this.props.deleteFavorite(l)}/>
                    </div>
                )}

            </div>
        );
    }
}

