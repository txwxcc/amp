import React, {Component} from 'react';
import {ListingView} from "./components/ListingView";
import {FavoriteView} from "./components/FavoriteView";
import api from "../../service/api";


export class OutputDisplay extends Component {
    constructor(props) {
        super(props);

        this.state = {listings: [], favorite: []};
        this.props.callback(problem => this.handler(problem));

        api.favorite.all().then(res => {
            this.setState({favorite: res.favorite})
        });
    }

    createFavorite(favorite) {
        api.favorite.add({list: favorite})
            .then(e => api.favorite.all()
                .then(e => this.setState({favorite: e.favorite}))
            );
    }

    deleteFavorite(favorite) {
        api.favorite.remove(favorite._id)
            .then(e => api.favorite.all()
                .then(e => this.setState({favorite: e.favorite}))
            );
    }

    markAsFavorite(listing) {
        this.setState({
            listings: this.state.listings.filter(l => l !== listing)
        });
        this.createFavorite(listing)
    }

    handler(problem) {
        const field = problem.aspects.filter(a =>
            a.attributes !== undefined && a.attributes.length > 0);

        const min = field.reduce((acc, curr) => {
            if (acc > curr.attributes.length)
                return curr.attributes.length;
            else
                return acc
        }, Number.MAX_VALUE);

        const listings = field
            .map(a => {
                let list = [];
                const attribs = [...a.attributes];

                for (let i = 0; i < min; i++)
                    list.push(Math.random());

                return list.map(r => ({
                    aspect: a.name,
                    attribute: attribs.splice(Math.floor(r * attribs.length), 1)[0]
                }));
            })
            .reduce((acc, curr) =>
                acc.map((e, i) => [e, curr[i]].flat())
            );
        this.setState({listings: listings})
    }

    render() {
        return (
            <div className="OutputDisplay">
                <FavoriteView
                    favorite={this.state.favorite}
                    deleteFavorite={favorite => deleteFavorite(favorite)}
                />
                {this.state.listings.map((l, i) =>
                    <ListingView
                        key={i}
                        listing={l}
                        markAsFavorite={listing => this.markAsFavorite(listing)}
                    />)}
            </div>
        );
    }
}

