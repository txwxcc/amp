const remove = name => id =>
    fetch(`/api/${name}/${id}`, {
        method: 'DELETE',
        headers: {'Content-Type': 'application/json'}
    }).then(data => data.json());

const update = name => (id, body) =>
    fetch(`/api/${name}/${id}`, {
        method: 'PUT',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(body)
    }).then(data => data.json());

const add = name => (body) =>
    fetch(`/api/${name}/`, {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(body)
    }).then(data => data.json());

const all = name => () =>
    fetch(`/api/${name}/`, {
        method: 'GET',
        headers: {'Content-Type': 'application/json'}
    }).then(data => data.json());

const get = name => (id) =>
    fetch(`/api/${name}/${id}`, {
        method: 'GET',
        headers: {'Content-Type': 'application/json'}
    }).then(data => data.json());

export default {
    problems: {
        all: all('problems'),
        add: add('problems'),
        get: get('problem'),
        update: update('problem'),
        remove: remove('problem')
    },
    favorite: {
        all: all('favorite'),
        add: add('favorite'),
        get: get('favorite'),
        update: update('favorite'),
        remove: remove('favorite')
    }
}
