const mongoose = require('mongoose');

mongoose.connect('mongodb://127.0.0.1:27017/amp');

module.exports.Problem = mongoose.model('Problem', new mongoose.Schema({
    name: String,
    aspects: [{
        name: String,
        attributes: [String]
    }]
}));

module.exports.Favorite = mongoose.model('Favorite', new mongoose.Schema({
    list: [{
        aspect: String,
        attribute: String
    }]
}));
