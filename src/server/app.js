const express = require('express');
const bodyParser = require('body-parser');
const api = require('./api');


const app = express();

app.use(express.static('pub'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use("/api", api);

app.listen(8080, () => console.log("Server started on :8080"));
