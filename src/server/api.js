const express = require('express');
const database = require('./database.js');


const api = express.Router();

const send = (res, name) => (err, result) => {
    if (err)
        res.json({error: true});
    else if (name === undefined)
        res.json({error: false});
    else {
        const json = {error: false};
        json[name] = result;

        res.json(json);
    }
};

api.route('/problems')
    .get((req, res) =>
        database.Problem.find(send(res, 'problems')))
    .post((req, res) => {
        const problem = new database.Problem();
        problem.name = req.body.name;
        problem.save(send(res));
    });

api.route('/favorite')
    .get((req, res) =>
        database.Favorite.find(send(res, 'favorite')))
    .post((req, res) => {
        const favorite = new database.Favorite();
        favorite.list = req.body.list;
        favorite.save(send(res));
    });

api.route('/problem/:problem_id')
    .get((req, res) => {
        database.Problem.findById(req.params.problem_id, send(res, 'problem'));
    })
    .put((req, res) => {
        database.Problem.findById(req.params.problem_id,(err, problem) => {
            if (err)
                res.json({error: true});
            else {
                problem.name = req.body.name;
                problem.aspects = req.body.aspects;
                problem.save(send(res))
            }
        });
    })
    .delete((req, res) => {
        database.Problem.remove({_id: req.params.problem_id}, send(res));
    });

api.route('/favorite/:favorite_id')
    .get((req, res) => {
        database.Favorite.findById(req.params.favorite_id, send(res, 'favorite'));
    })
    .put((req, res) => {
        database.Favorite.findById(req.params.favorite_id,(err, favorite) => {
            if (err)
                res.json({error: true});
            else {
                favorite.list = req.body.list;
                favorite.save(send(res))
            }
        });
    })
    .delete((req, res) => {
        database.Favorite.remove({_id: req.params.favorite_id}, send(res));
    });

module.exports = api;
