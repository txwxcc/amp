const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = [{
    name: "client",
    entry: "./src/client/app.js",
    output: {
        filename: "pub/client.js"
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: "pub/index.html",
            template: "src/client/index.html",
            inject: false
        })
    ],
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
                loader: "babel-loader"
            }
        }, {
            test: /\.css$/,
            exclude: /node_modules/,
            use: ["style-loader", "css-loader"]
        }]
    }
}, {
    name: "server",
    entry: "./src/server/app.js",
    output: {
        filename: "server.js"
    },
    target: "node"
}];